import * as Immutable from "immutable";

let initialState = Immutable.fromJS({
    todos: [],
    interpolationTest: {
        interpolationOne: {
           value: 'int 1'
        },
        interpolationTwo: {
             value: 'int 2'
        }
    }
});

export { initialState };