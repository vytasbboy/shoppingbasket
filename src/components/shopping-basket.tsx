import * as React from "react";
import { ComponentState } from "react-state-rxjs"

import { BasketStateActions } from "./actions/basket.actions";
import { ShoppingItem } from "./shopping-items/shopping-item.base";
import { Apple } from "./shopping-items/apple";
import { Banana } from "./shopping-items/banana";
import { Orange } from "./shopping-items/orange";
import { Papaya } from "./shopping-items/papaya";

@ComponentState(BasketStateActions)
export class ShoppingBasket extends React.Component<any, any> {

    actions: BasketStateActions;

    quantity: HTMLInputElement;
    item: HTMLSelectElement;
    availableItems: ShoppingItem[] = [];

    constructor() {
        super();
        this.availableItems.push(new Apple());
        this.availableItems.push(new Orange());
        this.availableItems.push(new Banana());
        this.availableItems.push(new Papaya());
    }

    render() {

        const selectItems = this.availableItems.map((item, index) => {
            return <option key={item.id.toString()} value={item.id}>{item.name} are {item.price} ct each</option>
        });

        return (

            <div className="container">
                <form className="form-inline">
                    <select
                        className="form-control mb-2 mr-sm-2 mb-sm-0"
                        ref={input => this.item = input}
                        name="item"
                        id="inlineFormInput">
                        {selectItems}
                    </select>
                    <input type="number" min="1" name="quantity" ref={input => this.quantity = input} className="form-control mb-2 mr-sm-2 mb-sm-0" id="inlineFormInput" placeholder="Quantity" />

                    <button type="button" className="btn btn-primary" onClick={this.addItem.bind(this)}>Submit</button>
                </form>
                <br />
                <table className="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Price / unit</th>
                            <th>Quantity</th>
                            <th>Total price</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getBasket()}
                    </tbody>
                </table>
                <hr></hr>
                <div className="text-right"><strong>Total:</strong> {this.actions.totalAsync} Euro</div>
            </div>
        )
    }

    deleteItem(index: number) {
        this.actions.deleteTodo(index);
    }

    addItem() {
        if (this.quantity.value === '') {
            return;
        }

        const itemToAdd = this.availableItems[this.availableItems.findIndex(item => item.id === parseInt(this.item.value))];

        this.actions.addItem(itemToAdd, parseInt(this.quantity.value));

        this.quantity.value = '';
    }

    private getBasket() {
        let shoppingItems = this.actions.todosAsync.map((item, index) => {
            return (<tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{item.get('name')}</td>
                <td>{item.get('price')} ct</td>
                <td>{item.get('quantity')}</td>
                <td>{item.get('totalPrice')} Euro</td>
                <td><button onClick={() => this.deleteItem(index)}>X</button></td>
            </tr>)
        }) as any;

        if (shoppingItems.length === 0) {
            shoppingItems = (<tr><td className="text-center" colSpan={6}>Basket is empty</td></tr>);
        }

        return shoppingItems;
    }
}