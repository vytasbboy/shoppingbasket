export class ShoppingItemModel {
    id: number;
    name: string;
    price: number;
    quantity: number;
    totalPrice: number;
    discount: number;
}