import * as Immutable from 'immutable';
import { HasStore, InjectStore, Store } from "react-state-rxjs";

import { ShoppingItemModel } from "./shopping-basket.model";
import { ShoppingItem } from "../shopping-items/shopping-item.base";

@InjectStore('todos')
export class BasketStateActions implements HasStore<any> {

    store: Store<any>;
    state: any;

    addItem(itemToAdd: ShoppingItem, quantity: number) {
        this.store.update((state: Immutable.List<any>) => {
            const existingItemIndex = state.findIndex((item: Immutable.Map<any, number>) => item.get('id') === itemToAdd.id);
            if (existingItemIndex !== -1) {
                const totalQuantity = state.getIn([existingItemIndex, 'quantity']) + quantity;
                var payedAmount = itemToAdd.getPrice(totalQuantity);

                state.updateIn([existingItemIndex, 'quantity'], (quantity) => totalQuantity);
                state.updateIn([existingItemIndex, 'totalPrice'], (totalPrice) => payedAmount.price);
                state.updateIn([existingItemIndex, 'discount'], (discount) => payedAmount.discount);
            } else {
                const payedAmount = itemToAdd.getPrice(quantity);

                state.push(Immutable.fromJS({
                    id: itemToAdd.id,
                    name: itemToAdd.name,
                    price: itemToAdd.price,
                    quantity: quantity,
                    totalPrice: payedAmount.price,
                    discount: payedAmount.discount
                }));
            }
        });
    }

    get totalAsync() {
        return this.store.map((state) => {
            const totalPrice = state.map((entry: any) => entry.get('totalPrice')).reduce((prev: number, current: number) => prev + current);
            return !totalPrice ? 0 : Math.round(totalPrice  * 100) / 100;
        });
    }

    deleteTodo(index: number) {
        this.store.update(state => {
            state.delete(index);
        }, false);
    }

    get todosAsync() {
        return this.store.map((state) => {
            return state.toArray();
        });
    }
}