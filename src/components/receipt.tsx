import * as React from "react";
import { ComponentState } from "react-state-rxjs"

import { BasketStateActions } from "./actions/basket.actions";
import { ShoppingItem } from "./shopping-items/shopping-item.base";
import { Apple } from "./shopping-items/apple";
import { Banana } from "./shopping-items/banana";
import { Orange } from "./shopping-items/orange";
import { Papaya } from "./shopping-items/papaya";

@ComponentState(BasketStateActions)
export class Receipt extends React.Component<any, any> {

    actions: BasketStateActions;

    quantity: HTMLInputElement;
    item: HTMLSelectElement;
    availableItems: ShoppingItem[] = [];

    render() {
        return (
            <div className="container">
                <br />
                <h3>Receipe</h3>
                <table className="table">
                    <thead>
                        <tr>
                            <th colSpan={4}>Date: {new Date(Date.now()).toLocaleDateString('de')} {new Date(Date.now()).toLocaleTimeString('de')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.getBasket()}
                    </tbody>
                </table>
                <hr></hr>
                <div className="text-right"><strong>Total:</strong> {this.actions.totalAsync} Euro</div>
            </div>
        )
    }

    private getBasket() {
        let shoppingItems = this.actions.todosAsync.map((item, index) => {
            const discount = item.get('discount') > 0 ? ` (discount: -${item.get('discount')} Euro)` : '';
            return (<tr key={index}>
                <th scope="row">{index + 1}</th>
                <td>{item.get('name')}</td>
                <td>{item.get('quantity')} units ({item.get('price')} ct per unit)</td>
                <td>{item.get('totalPrice')} Euro{discount}</td>
            </tr>)
        }) as any;

        if (shoppingItems.length === 0) {
            shoppingItems = (<tr><td className="text-center" colSpan={4}>Basket is empty</td></tr>);
        }

        return shoppingItems;
    }
}