import { ShoppingItem, IPrice } from "./shopping-item.base";

export class Orange extends ShoppingItem {

    id =  2;
    name = 'Orange';
    hasSpecialOffer = false;
    specialOfferName = '';
    price = 30;

     totalPrice(quantity: number): IPrice {
        return {
            price: this.price * quantity,
            discount: 0
        }
    };
}