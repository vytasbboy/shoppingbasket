import { ShoppingItem, IPrice } from "./shopping-item.base";

export class Apple extends ShoppingItem {

    id = 1;
    name = 'Apple';
    hasSpecialOffer = false;
    specialOfferName = '';
    price = 25;

    totalPrice(quantity: number): IPrice {
        return {
            price: this.price * quantity,
            discount: 0
        }
    };
}