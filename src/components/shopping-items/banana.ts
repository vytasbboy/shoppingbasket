import { ShoppingItem, IPrice } from "./shopping-item.base";

export class Banana extends ShoppingItem {

    id = 3;
    name = 'Banana';
    hasSpecialOffer = false;
    specialOfferName = '';
    price = 15;

     totalPrice(quantity: number): IPrice {
        return {
            price: this.price * quantity,
            discount: 0
        }
    };
}