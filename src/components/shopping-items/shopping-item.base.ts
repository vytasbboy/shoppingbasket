export abstract class ShoppingItem {
    abstract id: number;
    abstract specialOfferName: string;
    abstract hasSpecialOffer: boolean;
    abstract name: string;
    abstract price: number;
    abstract totalPrice(quantity: number): IPrice;

    getPrice(quantity: number): IPrice {
        let totalPrice = this.totalPrice(quantity);
        totalPrice.price = totalPrice.price / 100;
        totalPrice.discount = totalPrice.discount / 100;

        return totalPrice;
    }
}

export interface IPrice {
    price: number;
    discount: number
}