import { ShoppingItem, IPrice } from "./shopping-item.base";

export class Papaya extends ShoppingItem {

    id = 4;
    name = 'Papaya';
    hasSpecialOffer = true;
    specialOfferName = 'three for the price of two';
    price = 50;

    totalPrice(quantity: number): IPrice {
        const specialOfferItems = Math.floor(quantity / 3) * 2;
        const restOfItems = quantity % 3;
        const wholePrice = specialOfferItems * this.price + restOfItems * this.price;

        return {
            price: wholePrice,
            discount: quantity * this.price - wholePrice
        };
    };
}