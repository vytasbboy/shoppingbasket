import * as React from "react";

import { Link, Route, Router, BrowserRouter } from 'react-router-dom';

import { Receipt } from "./receipt";
import { StateHistoryComponent } from "react-state-rxjs";
import { ShoppingBasket } from "./shopping-basket";

export class Main extends React.Component<any, undefined> {
    render() {

        const links =
            <nav className="nav">
                <li className="nav-item">
                    <Link className="nav-link" to="/" title="Todos">Shopping basket</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/router-history-test" title="Router history Test">Receipe</Link>
                </li>
            </nav>

        return (
            <div>
                <div className="container">
                    <Router history={this.props.history}>
                        <div>
                            {links}
                            <Route path="/router-history-test" component={Receipt} />
                            <Route exact path="/" component={ShoppingBasket} />
                        </div>
                    </Router>
                </div>
                <StateHistoryComponent routerHistory={this.props.history} />
            </div>
        )
    }
}